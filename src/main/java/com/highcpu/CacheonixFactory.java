package com.highcpu;

import cacheonix.Cacheonix;
import cacheonix.ShutdownMode;
import cacheonix.cache.Cache;

import java.util.concurrent.ConcurrentMap;

public class CacheonixFactory {
    private final static String DEFAULT_CONFIG = "cacheonix-config2.xml";
    private Cacheonix cacheonix;

    public CacheonixFactory(String config) {
        cacheonix = Cacheonix.getInstance(config);
    }

    public <K extends java.io.Serializable, V extends java.io.Serializable> ConcurrentMap<K, V> getCache(String name) {
        Cache<K, V> cache = cacheonix.getCache(name);
        if (cache == null) {
            throw new RuntimeException("Cache initialization failed");
        }
        return cache;
    }

    public void shutdown() {
        cacheonix.shutdown(ShutdownMode.GRACEFUL_SHUTDOWN, true);
    }
}
