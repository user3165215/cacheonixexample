package com.highcpu;

import org.junit.Test;

import java.util.concurrent.ConcurrentMap;

public class TestCacheFactory {
    private static final String TEST_CACHE = "testcache";
    private static ConcurrentMap<String, String> cache;
    private static final long SLEEP_TIME = 2000000l;

    @Test
    public void testInfiniteForwardMarker() throws Exception {
        CacheonixFactory factory = new CacheonixFactory("cacheonix-config.xml");
        Thread.sleep(1000000l);
        factory.shutdown();
    }

    @Test
    public void testInfiniteForwardMarker2() throws Exception {
        CacheonixFactory factory = new CacheonixFactory("cacheonix-config2.xml");
        Thread.sleep(1000000l);
        factory.shutdown();
    }

    @Test
    public void testInfiniteForwardMarker3() throws Exception {
        CacheonixFactory factory = new CacheonixFactory("cacheonix-config3.xml");
        Thread.sleep(1000000l);
        factory.shutdown();
    }

  /*  @Test
    public void testCacheFactory() throws Exception {
        //Thread.sleep(SLEEP_TIME);
        System.out.println("----------------------------------- Putting first item to cache: ");
        cache.put("one", "two");
        Thread.sleep(SLEEP_TIME);
        System.out.println("----------------------------------- Putting second item to cache: ");
        cache.put("three", "four");
    }*/
}
